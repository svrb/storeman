# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'user.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(591, 377)
        self.del_btn = QtWidgets.QPushButton(Dialog)
        self.del_btn.setGeometry(QtCore.QRect(0, 90, 90, 33))
        self.del_btn.setObjectName("del_btn")
        self.add_btn = QtWidgets.QPushButton(Dialog)
        self.add_btn.setGeometry(QtCore.QRect(100, 90, 90, 33))
        self.add_btn.setObjectName("add_btn")
        self.save_btn = QtWidgets.QPushButton(Dialog)
        self.save_btn.setGeometry(QtCore.QRect(360, 330, 90, 33))
        self.save_btn.setObjectName("save_btn")
        self.num_field = QtWidgets.QSpinBox(Dialog)
        self.num_field.setGeometry(QtCore.QRect(220, 90, 54, 31))
        self.num_field.setMinimum(1)
        self.num_field.setMaximum(1000)
        self.num_field.setObjectName("num_field")
        self.material_field = QtWidgets.QLineEdit(Dialog)
        self.material_field.setGeometry(QtCore.QRect(320, 90, 231, 31))
        self.material_field.setObjectName("material_field")
        self.logout_btn = QtWidgets.QPushButton(Dialog)
        self.logout_btn.setGeometry(QtCore.QRect(10, 10, 90, 33))
        self.logout_btn.setObjectName("logout_btn")
        self.username_label = QtWidgets.QLabel(Dialog)
        self.username_label.setGeometry(QtCore.QRect(501, 20, 71, 20))
        self.username_label.setObjectName("username_label")
        self.msg_label = QtWidgets.QLabel(Dialog)
        self.msg_label.setGeometry(QtCore.QRect(151, 15, 261, 21))
        self.msg_label.setAlignment(QtCore.Qt.AlignCenter)
        self.msg_label.setObjectName("msg_label")
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(0, 50, 591, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.records_table = QtWidgets.QTableWidget(Dialog)
        self.records_table.setGeometry(QtCore.QRect(50, 140, 471, 181))
        self.records_table.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.records_table.setColumnCount(5)
        self.records_table.setObjectName("records_table")
        self.records_table.setRowCount(0)
        self.remove_btn = QtWidgets.QPushButton(Dialog)
        self.remove_btn.setGeometry(QtCore.QRect(140, 330, 90, 33))
        self.remove_btn.setObjectName("remove_btn")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(420, 65, 21, 20))
        self.label.setObjectName("label")
        self.line_2 = QtWidgets.QFrame(Dialog)
        self.line_2.setGeometry(QtCore.QRect(290, 70, 3, 61))
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.line_3 = QtWidgets.QFrame(Dialog)
        self.line_3.setGeometry(QtCore.QRect(200, 70, 3, 61))
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(200, 70, 62, 17))
        self.label_2.setObjectName("label_2")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.del_btn.setText(_translate("Dialog", "خروج"))
        self.add_btn.setText(_translate("Dialog", "ورود"))
        self.save_btn.setToolTip(_translate("Dialog", "<html><head/><body><p>تایید و ذخیره اطلاعات</p></body></html>"))
        self.save_btn.setText(_translate("Dialog", "ثبت"))
        self.material_field.setPlaceholderText(_translate("Dialog", "نام ماده"))
        self.logout_btn.setText(_translate("Dialog", "خروج از حساب"))
        self.username_label.setText(_translate("Dialog", "username"))
        self.msg_label.setText(_translate("Dialog", "خوش آمدید"))
        self.remove_btn.setToolTip(_translate("Dialog", "<html><head/><body><p dir=\"rtl\" align=\"right\">برای حذف کردن سطرهای موردنظر را از جدول بالا انتخاب کنید</p></body></html>"))
        self.remove_btn.setText(_translate("Dialog", "حذف"))
        self.label.setText(_translate("Dialog", "ماده"))
        self.label_2.setText(_translate("Dialog", "تعداد"))
