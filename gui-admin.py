#!/usr/bin/python

from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem ,QCompleter ,  QInputDialog
from PyQt5 import QtCore , QtWidgets , QtGui
import sys , hashlib , os
import sqlite3
import admin , loginadmin
import log , userman , material
import db_err

class database:
    def __init__(self):
        self.conn=sqlite3.connect("db.sql")
        self.db = self.conn.cursor()
        
    def refresh_user_list(self):
        self.db.execute("select user from user;")
        self.conn.commit()
        self.user_list = list()
        for i in self.db.fetchall():
            self.user_list.append(i[0])
            

    def refresh_material_list(self):
        self.db.execute("select material,max from material;")
        self.conn.commit()
        self.material_list = list()
        self.material_dict = dict()
        for i in  self.db.fetchall():
            self.material_list.append(i[0])
            self.material_dict[i[0]] = i[1]
            
    
    def auth_db(self):
        hash_md5 = hashlib.md5()
        with open("./db.sql", "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        hf=hash_md5.hexdigest()
        fhf=hashlib.sha256((hf+'03e9488a5f1b03016541da25237cd401df407a940960bdbb81e831bee2897549e468bcb98ec2b109f9a77515f9fd7bf5f88e8b23a0f090166e7c0d4755d90c75').encode('utf-8')).hexdigest()
        with  open ("sign" , "r") as sign_file:
            s=sign_file.read(64)
        if not s == fhf:
            return False
        else:
            return True
                    
        
    def check_pass(self, username , password):
        self.db.execute("select password from user where user like ? ;" , (username,))
        self.conn.commit()
        pass_string = self.db.fetchall()[0][0]
        pass_hash , salt = pass_string.split(':')
        
        if password and hashlib.md5((password + salt).encode('UTF-8')).hexdigest() == pass_hash:
            return True
        else:
            return False
    
    def search(self , keyword , mode , start_date=" " , end_date=" " ):
        if mode == "user" :
            self.db.execute("select user,material,number,dir,time from list where user like ? ;" , (keyword,))
        if mode == "material":
            self.db.execute("select user,material,number,dir,time from list where material like ? ;" , (keyword,))
        if mode == "date":
            self.db.execute("select user,material,number,dir,time from list where time BETWEEN ? AND ? ;" , (start_date , end_date) )
        self.conn.commit()
        return self.db.fetchall()
    
    def change_pass(self , username , newpass):
        salt=os.urandom(16).hex()
        pass_hash = hashlib.md5((newpass+salt).encode('UTF-8') ).hexdigest()
        pass_string = pass_hash+':'+salt
        self.db.execute("UPDATE user set password = ? where user like ? ; " , (pass_string , username))
        self.conn.commit()
        self.sign_db()
        
    def remove_user(self,username):
        self.db.execute("delete from user where user like ? ; " , (username,))
        self.conn.commit()
        self.sign_db()
    
    def add_user(self , username , password):
        salt=os.urandom(16).hex()
        pass_hash = hashlib.md5((password+salt).encode('UTF-8') ).hexdigest()
        pass_string = pass_hash+':'+salt
        self.db.execute("insert into user (user , password ) values ( ? , ? ); " , (username , pass_string))
        self.conn.commit()
        self.sign_db()

    def add_material( self, new_material_name , new_material_max):
        self.db.execute("insert into material (material , max ) values ( ? , ? ); " , (new_material_name , new_material_max))
        self.conn.commit()
        self.sign_db()
        
    def remove_material(self , material):
        self.db.execute("delete from material where material like ? ; " , (material,))
        self.conn.commit()
        self.sign_db()
    
    def change_material_max(self , material , new_max):
        self.db.execute("UPDATE material set max = ? where material like ? ;" , (new_max , material))
        self.conn.commit()
        self.sign_db()

    def refresh_balance(self): # create or refresh balance
        self.balance = dict()
        for material in self.material_list:
            self.db.execute("select  number, dir from list where material = ?;" , (material,) )
            self.conn.commit()
            result = self.db.fetchall()
            #if result == [] :
                #return False
            number_in_shop = 0
            for number , action in result :
                if action == "I":
                    number_in_shop += number
                if action == "E" :
                    number_in_shop -= number
            self.balance[material] = number_in_shop

    def sign_db(self):
        hash_md5 = hashlib.md5()
        with open("./db.sql", "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        hf=hash_md5.hexdigest()
        fhf=hashlib.sha256((hf+'03e9488a5f1b03016541da25237cd401df407a940960bdbb81e831bee2897549e468bcb98ec2b109f9a77515f9fd7bf5f88e8b23a0f090166e7c0d4755d90c75').encode('utf-8')).hexdigest()
        with open ("sign" , "w") as sign_file:
            sign_file.write(fhf)

        
        
#GUI class section start
class db_err_class(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = db_err.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.exit.clicked.connect(self.close_app)
    def close_app(self):
        app.exit(2)
        
class loginadmin_win_class(QDialog):
    loginsignal = QtCore.pyqtSignal(str)
    def __init__(self):
        super().__init__()
        self.ui = loginadmin.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.login_btn.clicked.connect(self.login)
        
    def login(self):
        self.loginsignal.emit(self.ui.admin_pass.text())

class admin_win_class(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = admin.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.log_btn.clicked.connect(self.log_window)
        self.ui.user_btn.clicked.connect(self.userman_window)
        self.ui.material_btn.clicked.connect(self.material_window)
        self.ui.exit_btn.clicked.connect(self.exit)
        
    def log_window(self):
        log_win_obj.show()
        
    def userman_window(self):
        userman_win_obj.refresh_table()
        userman_win_obj.show()
    
    def material_window(self):
        materialman_win_obj.refresh_table()
        materialman_win_obj.show()
        
    def exit(self):
        app.exit(0)
        
class log_win_class(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = log.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.log_table.setHorizontalHeaderLabels(('کاربر','ماده','تعداد','عملیات' ,'تاریخ'))
        self.ui.log_table.setSortingEnabled(True)
        header = self.ui.log_table.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)
        self.ui.search_btn.clicked.connect(self.search)
        db.refresh_user_list()
        db.refresh_material_list()
        completer = QCompleter( db.user_list + db.material_list )
        self.ui.keyword.setCompleter(completer)
        
        
    def search(self):
        self.ui.msg_label.setText(" ")
        keyword = self.ui.keyword.text()
        if keyword == '' and not self.ui.radio_date.isChecked():
            self.ui.msg_label.setText("عبارت جست وجو وارد نشده!")
            return False
        if self.ui.radio_user.isChecked():
            table = db.search(keyword , "user")
        if self.ui.radio_material.isChecked():
            table = db.search(keyword , "material")
        if self.ui.radio_date.isChecked():
            start_date=self.ui.start_date.text()
            end_date=self.ui.end_date.text()
            table = db.search(" " , "date" , start_date , end_date)
        if table == []:
            self.ui.msg_label.setText("موردی یافت نشد!")
            return False
        self.ui.log_table.clearContents()
        self.ui.log_table.setRowCount(0)
        row_count = 0
        for row in table :
            self.ui.log_table.setRowCount(row_count+1)
            collumn_count = 0
            for collumn in row :
                if collumn_count == 3 and collumn == "I":
                    collumn = "ورود"
                if collumn_count == 3 and collumn == "E":
                    collumn = "خروج"
                item = QTableWidgetItem( str(collumn) )
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.log_table.setItem(row_count , collumn_count , item)
                collumn_count += 1
            row_count += 1
        
        

class userman_win_class(QDialog):
    def __init__(self):
        #change_pass_signal = QtCore.pyqtSignal(str , str)
        super().__init__()
        self.ui = userman.Ui_Dialog()
        self.ui.setupUi(self)
        #self.ui.user_table.setColumnCount(0)
        self.ui.user_table.setHorizontalHeaderLabels(('کاربران',))
        self.ui.change_pass_btn.clicked.connect(self.change_pass)
        self.ui.add_user_btn.clicked.connect(self.add_user)
        self.ui.remove_user_btn.clicked.connect(self.remove_user)
        
    def change_pass(self):
        user = self.ui.user_table.currentItem().text()
        get_pass_dialog = QInputDialog(self)
        get_pass_dialog.setOkButtonText("تایید")
        get_pass_dialog.setCancelButtonText("انصراف")
        get_pass_dialog.setTextEchoMode(QtWidgets.QLineEdit.Password)
        get_pass_dialog.setLabelText("پسورد جدید "+user+" را وارد کنید ")
        get_pass_dialog.setWindowTitle("پسورد جدید ")
        get_pass_dialog.resize(250,100)
        ok = get_pass_dialog.exec_()
        if ok :
            if get_pass_dialog.textValue() != '' :
                newpass = get_pass_dialog.textValue()
                db.change_pass(user , newpass)
                self.ui.msg_label.setText("پسورد "+ user + " عوض شد")
            else:
                self.ui.msg_label.setText("پسورد نمی تواند خالی باشد")
        
    def remove_user(self):
        user = self.ui.user_table.currentItem().text()
        if user == "admin" :
            self.ui.msg_label.setText("کاربر ادمین را نمی توان حذف کرد")
            return False
        
        db.remove_user(user)
        self.refresh_table()
        self.ui.msg_label.setText("کاربر  " + user + " حذف شد")
        
    def add_user(self):
        user = self.ui.new_username.text()
        if user == "" : 
            self.ui.msg_label.setText("نام کاربری وارد نشده است")
            return False
        
        password = self.ui.new_userpass.text()
        if password == "":
            self.ui.msg_label.setText("پسورد کاربر نمی تواند خالی باشد")
            return False
        db.add_user(user , password)
        self.ui.msg_label.setText("کاربر افزوده شد")
        self.refresh_table()
        self.ui.new_username.setText("")
        self.ui.new_userpass.setText("")

            
    def refresh_table(self): #refresh or create table 
        self.ui.user_table.clearContents()
        self.ui.user_table.setRowCount(0)
        db.refresh_user_list()
        for i in range(len(db.user_list)):
            #row_count = self.ui.user_table.rowCount() 
            #self.ui.user_table.setRowCount(row_count + 1)
            self.ui.user_table.setRowCount(i+1)
            user_item = QTableWidgetItem( db.user_list[i] )
            user_item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.user_table.setItem(i, 0 , user_item)
            
class materialman_win_class(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = material.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.material_table.setHorizontalHeaderLabels(("ماده" , "حد آستانه" ,"موجودی"))
        header = self.ui.material_table.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.ui.material_table.setSortingEnabled(True)
        self.ui.add_material_btn.clicked.connect(self.add_material)
        self.ui.remove_material_btn.clicked.connect(self.remove_material)
        self.ui.change_max_btn.clicked.connect(self.change_material_max)
        
    def refresh_table(self): #refresh or create table 
        self.ui.material_table.clearContents()
        self.ui.material_table.setRowCount(0)
        db.refresh_material_list()
        db.refresh_balance()
        for i in range(len(db.material_list)):
            self.ui.material_table.setRowCount(i+1)
            material_item = QTableWidgetItem( db.material_list[i] )
            max_item = QTableWidgetItem( str(db.material_dict[db.material_list[i]]) )
            balance_item = QTableWidgetItem( str(db.balance[db.material_list[i]]) )
            material_item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            max_item.setFlags(QtCore.Qt.ItemIsSelectable)
            balance_item.setFlags(QtCore.Qt.ItemIsSelectable)
            if db.balance[db.material_list[i]] <= db.material_dict[db.material_list[i]] :
                material_item.setBackground(QtGui.QColor(255,0,0))
            self.ui.material_table.setItem(i, 0 , material_item)
            self.ui.material_table.setItem(i, 1 , max_item)
            self.ui.material_table.setItem(i, 2 , balance_item)
    
    def add_material(self):
        new_material_name = self.ui.new_material_name.text()
        if new_material_name == '':
            self.ui.msg_label.setText("نام ماده جدید را وارد کنید!")
            return False
        new_material_max = self.ui.new_material_max.value()
        db.add_material(new_material_name , new_material_max)
        self.refresh_table()

    def remove_material (self):
        row_must_be_remove =  self.ui.material_table.selectedIndexes()
        if not row_must_be_remove :
            self.ui.msg_label.setText("لطفا یک یا چند ماده را از جدول انتخاب کنید!")
            return False
        for i in row_must_be_remove:
            material = i.data()
            db.remove_material(material)
        self.refresh_table()
    
    def change_material_max(self):
        row_must_bechnge =  self.ui.material_table.selectedIndexes()
        if not row_must_bechnge :
            self.ui.msg_label.setText("لطفا یک یا چند ماده را از جدول انتخاب کنید!")
            return False
        for i in row_must_bechnge:
            material = i.data()
            get_max_dialog = QInputDialog(self)
            get_max_dialog.setOkButtonText("تایید")
            get_max_dialog.setCancelButtonText("انصراف")
            get_max_dialog.setInputMode(1) # its enable SPIN mode (defualt is text input)
            get_max_dialog.setLabelText("مقدار جدید "+material+" را وارد کنید ")
            get_max_dialog.setWindowTitle("مقدار جدید ")
            get_max_dialog.resize(250,100)
            ok = get_max_dialog.exec_()
            if ok :
                new_max = get_max_dialog.intValue()
                db.change_material_max(material, new_max)
        self.refresh_table()
        
def login(password):
    if db.check_pass("admin", password):
        loginadmin_win_obj.close()
        admin_win_obj.show()
    else:
        loginadmin_win_obj.ui.msg_label.setText(" کلمه عبور اشتباه است")
        
app = QApplication(sys.argv)
db = database()
db_err_obj = db_err_class()
loginadmin_win_obj = loginadmin_win_class()
admin_win_obj = admin_win_class() 
log_win_obj = log_win_class()
userman_win_obj = userman_win_class()
materialman_win_obj = materialman_win_class()
loginadmin_win_obj.loginsignal.connect(login)

if db.auth_db():
    loginadmin_win_obj.show()
else:
    db_err_obj.show()
    

sys.exit(app.exec_())
