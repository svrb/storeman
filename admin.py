# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'admin.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(314, 206)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.log_btn = QtWidgets.QPushButton(Dialog)
        self.log_btn.setGeometry(QtCore.QRect(190, 20, 90, 33))
        self.log_btn.setObjectName("log_btn")
        self.user_btn = QtWidgets.QPushButton(Dialog)
        self.user_btn.setGeometry(QtCore.QRect(190, 70, 90, 33))
        self.user_btn.setObjectName("user_btn")
        self.material_btn = QtWidgets.QPushButton(Dialog)
        self.material_btn.setGeometry(QtCore.QRect(190, 130, 90, 33))
        self.material_btn.setObjectName("material_btn")
        self.exit_btn = QtWidgets.QPushButton(Dialog)
        self.exit_btn.setGeometry(QtCore.QRect(20, 20, 90, 33))
        self.exit_btn.setObjectName("exit_btn")
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(140, 20, 20, 171))
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "پنل مدریت - نسخه ۱"))
        self.log_btn.setText(_translate("Dialog", "گزارش گیری"))
        self.user_btn.setText(_translate("Dialog", "کاربران"))
        self.material_btn.setText(_translate("Dialog", "لیست مواد"))
        self.exit_btn.setText(_translate("Dialog", "خروج"))
