# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'loginadmin.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(371, 300)
        self.admin_pass = QtWidgets.QLineEdit(Dialog)
        self.admin_pass.setGeometry(QtCore.QRect(70, 90, 231, 31))
        self.admin_pass.setEchoMode(QtWidgets.QLineEdit.Password)
        self.admin_pass.setObjectName("admin_pass")
        self.login_btn = QtWidgets.QPushButton(Dialog)
        self.login_btn.setGeometry(QtCore.QRect(150, 150, 90, 33))
        self.login_btn.setObjectName("login_btn")
        self.msg_label = QtWidgets.QLabel(Dialog)
        self.msg_label.setGeometry(QtCore.QRect(120, 230, 171, 21))
        self.msg_label.setText("")
        self.msg_label.setAlignment(QtCore.Qt.AlignCenter)
        self.msg_label.setObjectName("msg_label")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "پرتال مدریت - ورود"))
        self.admin_pass.setPlaceholderText(_translate("Dialog", "رمز مدیریت را وارد کنید"))
        self.login_btn.setText(_translate("Dialog", "ورود"))
