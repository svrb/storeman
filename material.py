# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'material.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(451, 423)
        self.new_material_name = QtWidgets.QLineEdit(Dialog)
        self.new_material_name.setGeometry(QtCore.QRect(202, 10, 231, 31))
        self.new_material_name.setObjectName("new_material_name")
        self.new_material_max = QtWidgets.QSpinBox(Dialog)
        self.new_material_max.setGeometry(QtCore.QRect(130, 10, 54, 31))
        self.new_material_max.setMinimum(1)
        self.new_material_max.setObjectName("new_material_max")
        self.add_material_btn = QtWidgets.QPushButton(Dialog)
        self.add_material_btn.setGeometry(QtCore.QRect(9, 10, 101, 33))
        self.add_material_btn.setObjectName("add_material_btn")
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(17, 50, 421, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.material_table = QtWidgets.QTableWidget(Dialog)
        self.material_table.setGeometry(QtCore.QRect(180, 100, 256, 311))
        self.material_table.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.material_table.setColumnCount(3)
        self.material_table.setObjectName("material_table")
        self.material_table.setRowCount(0)
        self.remove_material_btn = QtWidgets.QPushButton(Dialog)
        self.remove_material_btn.setGeometry(QtCore.QRect(50, 210, 90, 33))
        self.remove_material_btn.setObjectName("remove_material_btn")
        self.change_max_btn = QtWidgets.QPushButton(Dialog)
        self.change_max_btn.setGeometry(QtCore.QRect(50, 270, 90, 33))
        self.change_max_btn.setObjectName("change_max_btn")
        self.msg_label = QtWidgets.QLabel(Dialog)
        self.msg_label.setGeometry(QtCore.QRect(20, 70, 411, 20))
        self.msg_label.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.msg_label.setAlignment(QtCore.Qt.AlignCenter)
        self.msg_label.setObjectName("msg_label")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "پنل مدیریت نسخه ۱ - مدیریت مواد"))
        self.new_material_name.setPlaceholderText(_translate("Dialog", "نام ماده جدید"))
        self.add_material_btn.setText(_translate("Dialog", "افزودن ماده جدید"))
        self.remove_material_btn.setText(_translate("Dialog", "حذف ماده"))
        self.change_max_btn.setText(_translate("Dialog", "تغییر تعداد"))
        self.msg_label.setText(_translate("Dialog", "خوش آمدید"))
