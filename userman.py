# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'userman.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setEnabled(True)
        Dialog.resize(400, 300)
        self.user_table = QtWidgets.QTableWidget(Dialog)
        self.user_table.setGeometry(QtCore.QRect(40, 30, 111, 251))
        self.user_table.setColumnCount(1)
        self.user_table.setObjectName("user_table")
        self.user_table.setRowCount(0)
        self.new_username = QtWidgets.QLineEdit(Dialog)
        self.new_username.setGeometry(QtCore.QRect(240, 30, 113, 31))
        self.new_username.setObjectName("new_username")
        self.new_userpass = QtWidgets.QLineEdit(Dialog)
        self.new_userpass.setGeometry(QtCore.QRect(240, 70, 113, 31))
        self.new_userpass.setEchoMode(QtWidgets.QLineEdit.PasswordEchoOnEdit)
        self.new_userpass.setObjectName("new_userpass")
        self.add_user_btn = QtWidgets.QPushButton(Dialog)
        self.add_user_btn.setGeometry(QtCore.QRect(250, 120, 90, 33))
        self.add_user_btn.setObjectName("add_user_btn")
        self.remove_user_btn = QtWidgets.QPushButton(Dialog)
        self.remove_user_btn.setGeometry(QtCore.QRect(290, 250, 90, 33))
        self.remove_user_btn.setObjectName("remove_user_btn")
        self.change_pass_btn = QtWidgets.QPushButton(Dialog)
        self.change_pass_btn.setGeometry(QtCore.QRect(180, 250, 90, 33))
        self.change_pass_btn.setObjectName("change_pass_btn")
        self.msg_label = QtWidgets.QLabel(Dialog)
        self.msg_label.setGeometry(QtCore.QRect(210, 190, 171, 20))
        self.msg_label.setText("")
        self.msg_label.setAlignment(QtCore.Qt.AlignCenter)
        self.msg_label.setObjectName("msg_label")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "مدیریت کاربران -نسخه ۱"))
        self.new_username.setPlaceholderText(_translate("Dialog", "نام کاربر جدید"))
        self.new_userpass.setPlaceholderText(_translate("Dialog", "پسورد کاربرجدید"))
        self.add_user_btn.setText(_translate("Dialog", "افزودن کاربر"))
        self.remove_user_btn.setToolTip(_translate("Dialog", "<html><head/><body><p dir=\'rtl\' >کاربران را از جدول روبرو انتخاب کنید</p></body></html>"))
        self.remove_user_btn.setText(_translate("Dialog", "حذف کاربران"))
        self.change_pass_btn.setToolTip(_translate("Dialog", "<html><head/><body><p dir=\'rtl\'>گاربران را از جدول روبرو انتخاب کنید</p></body></html>"))
        self.change_pass_btn.setText(_translate("Dialog", "تغییر پسورد"))
