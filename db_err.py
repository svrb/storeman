# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'db_err.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(31, 90, 341, 20))
        self.label.setStyleSheet("color:red;")
        self.label.setObjectName("label")
        self.exit = QtWidgets.QPushButton(Dialog)
        self.exit.setGeometry(QtCore.QRect(160, 180, 90, 33))
        self.exit.setObjectName("exit")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "هشدار"))
        self.label.setText(_translate("Dialog", "هشدار: فایل دیتابیس توسط یک برنامه ناشناس ویرایش شده است!"))
        self.exit.setText(_translate("Dialog", "خروج"))
