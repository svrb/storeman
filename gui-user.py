#!/usr/bin/python

#insert into list (user,time,material,dir,number) values('user' ,datetime('now','localtime'), 'm3','i',10);
from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem ,QCompleter
from PyQt5 import QtCore , QtWidgets , QtGui
import sys , hashlib , os
import sqlite3
import login_win
import user_win , db_err

class database:
    def __init__(self):
        self.conn=sqlite3.connect("db.sql")
        self.db = self.conn.cursor()
        self.db.execute("select material,max from material;")
        self.conn.commit()
        self.material_list = list()
        self.material_dict = dict()
        for i in  self.db.fetchall():
            self.material_list.append(i[0])
            self.material_dict[i[0]] = i[1]
    
    def auth_db(self):
        hash_md5 = hashlib.md5()
        with open("./db.sql", "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        hf=hash_md5.hexdigest()
        fhf=hashlib.sha256((hf+'03e9488a5f1b03016541da25237cd401df407a940960bdbb81e831bee2897549e468bcb98ec2b109f9a77515f9fd7bf5f88e8b23a0f090166e7c0d4755d90c75').encode('utf-8')).hexdigest()
        with open ("sign" , "r") as sign_file:
            s=sign_file.read(64)
        if not s == fhf:
            return False
        else:
            return True
            
        
    def check_pass(self, username , password):
        if not username or not password:
            return False
        self.db.execute("select password from user where user like ? ;" , (username,))
        self.conn.commit()
        pass_string = self.db.fetchall()[0][0]
        pass_hash , salt = pass_string.split(':')
        
        if password and hashlib.md5((password + salt).encode('UTF-8')).hexdigest() == pass_hash:
            login_win_obj.ui.pass_line.setText("")
            return True
        else:
            return False

    def balance(self , material):
        self.db.execute("select  number, dir from list where material = ?;" , (material,) )
        self.conn.commit()
        result = self.db.fetchall()
        if result == [] :
            return False
        number_in_shop = 0
        for number , action in result :
            if action == "I":
                number_in_shop += number
            if action == "E" :
                number_in_shop -= number
        return number_in_shop
        
    def new_record(self, user, material  , number , stat ):
        self.db.execute("insert into list (user,time,material,dir,number) values(? , datetime('now','localtime' ) , ? , ? , ?);" , (user,material,stat,number) )
        self.conn.commit()
        self.sign_db()
        
    def sign_db(self):
        hash_md5 = hashlib.md5()
        with open("./db.sql", "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        hf=hash_md5.hexdigest()
        fhf=hashlib.sha256((hf+'03e9488a5f1b03016541da25237cd401df407a940960bdbb81e831bee2897549e468bcb98ec2b109f9a77515f9fd7bf5f88e8b23a0f090166e7c0d4755d90c75').encode('utf-8')).hexdigest()
        with open ("sign" , "w") as sign_file:
            sign_file.write(fhf)
        
#GUI class section start
class db_err_class(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = db_err.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.exit.clicked.connect(self.close_app)
    def close_app(self):
        app.exit(2)
        
class login_class(QDialog):
    switch_window = QtCore.pyqtSignal(str , str)
    def __init__(self):
        super().__init__()
        self.ui = login_win.Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle('ورود به سیستم - نسخه ۱')
        self.setWindowIcon(QtGui.QIcon("icon.png"))
        self.ui.login_btn.clicked.connect(self.login)
        self.ui.username_line.setFocus()
    def login(self):
        self.switch_window.emit(self.ui.username_line.text(), self.ui.pass_line.text())
        
        
class user_win_class(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = user_win.Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle('پنل کاربری - نسخه ۱')
        self.setWindowIcon(QtGui.QIcon("icon.png"))
        completer = QCompleter(db.material_list)
        self.ui.material_field.setCompleter(completer)
        self.ui.add_btn.clicked.connect(self.enter)
        self.ui.del_btn.clicked.connect(self.exit)
        self.ui.remove_btn.clicked.connect(self.remove_item_table)
        self.ui.save_btn.clicked.connect(self.save_table)
        self.ui.logout_btn.clicked.connect(logout)
        #self.ui.records_table.setColumnCount(5)
        self.ui.records_table.setHorizontalHeaderLabels(('کاربر', 'ماده','تعداد','عملیات',"موجودی"))  # set header text
        header = self.ui.records_table.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.ui.records_table.setSortingEnabled(True) # enable sort table
        self.temp_num_in_shop = dict() # its using for calculate in table before save in the db
        
    def enter(self):
        self.add_item_table( "ورود")
        
    def exit(self):
        self.add_item_table("خروج" )
        
    def add_item_table(self, stat):
        material = self.ui.material_field.text()
        try:
            db.material_list.index(material)
        except ValueError :
            self.ui.msg_label.setText("ماده پیدا نشد!")
            return False
        try:
            self.temp_num_in_shop[material]
        except KeyError:
            self.temp_num_in_shop[material] = db.balance(material) # its must not to be change every time so we save it to attribute (self.)
            
        row_count = self.ui.records_table.rowCount() 
        self.ui.records_table.setRowCount(row_count + 1)
        user = login_win_obj.ui.username_line.text()
        number = self.ui.num_field.value()
        if stat == "خروج":
            self.temp_num_in_shop[material] -= number
        if stat == "ورود":
            self.temp_num_in_shop[material] += number
            
        column_counter = 0
        for i in user , material , str(number) ,stat , str(self.temp_num_in_shop[material]):
            column_item = QTableWidgetItem(i)
            column_item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled) # disable edit mode
            if self.temp_num_in_shop[material] <= db.material_dict[material] :
                column_item.setBackground(QtGui.QColor(255,0,0))
            self.ui.records_table.closePersistentEditor(column_item)
            self.ui.records_table.setItem(row_count, column_counter , column_item)
            column_counter += 1

        self.ui.msg_label.setText("درخواست افزوده شد")
    def remove_item_table(self):
        row_must_be_deleted = self.ui.records_table.selectedIndexes() # get selected multi cells
        j=0
        for i in row_must_be_deleted:
            material = i.siblingAtColumn(1).data()
            number = int( i.siblingAtColumn(2).data() )
            action = i.siblingAtColumn(3).data()
            if action == "خروج" :
                self.temp_num_in_shop[material]  += number
            if action == "ورود":
                self.temp_num_in_shop[material] -= number
            self.ui.records_table.removeRow(i.row()-j) # for delete one row , index must be -1 
            j += 1
            
    def save_table(self):
        total_row = self.ui.records_table.rowCount()
        for row in range(total_row):
            columns = list()
            for column in range(4):
                columns.append(self.ui.records_table.item( 0 , column).text()) #save row=0 then remove row=0 so row=1 is set to row=0 and so on.
            if columns[3] == 'خروج' :
                columns[3] = 'E'
            else:
                columns[3] = "I"
            db.new_record(columns[0],columns[1],columns[2],columns[3])
            self.ui.records_table.removeRow(0)
        self.ui.msg_label.setText("ثبت شد!")
#GUI section end

def auth_user(username,password):
    if db.check_pass(username, password):
        user_win_obj.ui.username_label.setText(username)
        login_win_obj.close()
        user_win_obj.show()
        user_win_obj.ui.material_field.setFocus()
    else:
        login_win_obj.ui.login_status.setText("نام کاربری یا کلمه عبور اشتباه است")
        
def logout():
    user_win_obj.ui.records_table.clearContents()
    user_win_obj.ui.records_table.setRowCount(0)
    user_win_obj.close()
    login_win_obj.show()
    login_win_obj.ui.login_status.setText("")
    login_win_obj.ui.username_line.setText("")
    user_win_obj.ui.material_field.setText("")
    user_win_obj.ui.num_field.setValue(1)
    user_win_obj.ui.msg_label.setText("خوش آمدید")

app = QApplication(sys.argv)
db = database()
db_err_obj = db_err_class()
login_win_obj = login_class()
login_win_obj.switch_window.connect(auth_user)
if db.auth_db() :
    login_win_obj.show()
else:
    db_err_obj.show()
user_win_obj = user_win_class()
sys.exit(app.exec_())
