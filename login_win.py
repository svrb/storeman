# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.login_btn = QtWidgets.QPushButton(Dialog)
        self.login_btn.setGeometry(QtCore.QRect(180, 170, 90, 33))
        self.login_btn.setObjectName("login_btn")
        self.username_line = QtWidgets.QLineEdit(Dialog)
        self.username_line.setGeometry(QtCore.QRect(140, 60, 152, 31))
        self.username_line.setObjectName("username_line")
        self.pass_line = QtWidgets.QLineEdit(Dialog)
        self.pass_line.setGeometry(QtCore.QRect(140, 110, 152, 31))
        self.pass_line.setFocusPolicy(QtCore.Qt.TabFocus)
        self.pass_line.setEchoMode(QtWidgets.QLineEdit.Password)
        self.pass_line.setObjectName("pass_line")
        self.login_status = QtWidgets.QLabel(Dialog)
        self.login_status.setGeometry(QtCore.QRect(100, 240, 211, 20))
        self.login_status.setText("")
        self.login_status.setObjectName("login_status")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.login_btn.setText(_translate("Dialog", "ورود"))
        self.username_line.setPlaceholderText(_translate("Dialog", "نام کاربری"))
        self.pass_line.setPlaceholderText(_translate("Dialog", "رمز عبور"))
