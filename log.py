# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'log.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(552, 426)
        self.keyword = QtWidgets.QLineEdit(Dialog)
        self.keyword.setGeometry(QtCore.QRect(350, 10, 171, 31))
        self.keyword.setObjectName("keyword")
        self.radio_material = QtWidgets.QRadioButton(Dialog)
        self.radio_material.setGeometry(QtCore.QRect(470, 60, 71, 21))
        self.radio_material.setChecked(True)
        self.radio_material.setObjectName("radio_material")
        self.radio_user = QtWidgets.QRadioButton(Dialog)
        self.radio_user.setGeometry(QtCore.QRect(390, 60, 61, 21))
        self.radio_user.setObjectName("radio_user")
        self.log_table = QtWidgets.QTableWidget(Dialog)
        self.log_table.setGeometry(QtCore.QRect(20, 110, 511, 251))
        self.log_table.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.log_table.setColumnCount(5)
        self.log_table.setObjectName("log_table")
        self.log_table.setRowCount(0)
        self.log_table.horizontalHeader().setVisible(True)
        self.log_table.horizontalHeader().setCascadingSectionResizes(False)
        self.search_btn = QtWidgets.QPushButton(Dialog)
        self.search_btn.setGeometry(QtCore.QRect(220, 10, 90, 33))
        self.search_btn.setObjectName("search_btn")
        self.radio_date = QtWidgets.QRadioButton(Dialog)
        self.radio_date.setGeometry(QtCore.QRect(310, 60, 51, 21))
        self.radio_date.setObjectName("radio_date")
        self.start_date = QtWidgets.QDateEdit(Dialog)
        self.start_date.setGeometry(QtCore.QRect(190, 50, 110, 31))
        self.start_date.setDateTime(QtCore.QDateTime(QtCore.QDate(2020, 1, 1), QtCore.QTime(0, 0, 0)))
        self.start_date.setCalendarPopup(True)
        self.start_date.setDate(QtCore.QDate(2020, 1, 1))
        self.start_date.setObjectName("start_date")
        self.end_date = QtWidgets.QDateEdit(Dialog)
        self.end_date.setGeometry(QtCore.QRect(20, 50, 110, 31))
        self.end_date.setDateTime(QtCore.QDateTime(QtCore.QDate(2020, 1, 1), QtCore.QTime(0, 0, 0)))
        self.end_date.setCalendarPopup(True)
        self.end_date.setDate(QtCore.QDate(2020, 1, 1))
        self.end_date.setObjectName("end_date")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(150, 60, 21, 20))
        self.label.setObjectName("label")
        self.msg_label = QtWidgets.QLabel(Dialog)
        self.msg_label.setGeometry(QtCore.QRect(60, 390, 431, 20))
        self.msg_label.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.msg_label.setText("")
        self.msg_label.setAlignment(QtCore.Qt.AlignCenter)
        self.msg_label.setObjectName("msg_label")

        self.retranslateUi(Dialog)
        self.start_date.userDateChanged['QDate'].connect(self.radio_date.click)
        self.end_date.userDateChanged['QDate'].connect(self.radio_date.click)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "پنل مدیریت نسخه ۱ - گزارش گیری"))
        self.keyword.setPlaceholderText(_translate("Dialog", "مورد جست‌ و جو"))
        self.radio_material.setText(_translate("Dialog", "نام ماده"))
        self.radio_user.setText(_translate("Dialog", "کاربر"))
        self.search_btn.setText(_translate("Dialog", "جست وجو"))
        self.radio_date.setText(_translate("Dialog", "تاریخ"))
        self.start_date.setDisplayFormat(_translate("Dialog", "yyyy-MM-dd"))
        self.end_date.setDisplayFormat(_translate("Dialog", "yyyy-MM-dd"))
        self.label.setText(_translate("Dialog", "تا"))
